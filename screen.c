/* evilwm - Minimalist Window Manager for X
 * Copyright (C) 1999-2009 Ciaran Anscomb <evilwm@6809.org.uk>
 * see README for license and other details. */

#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "evilwm.h"
#include "log.h"

#ifdef INFOBANNER
Window info_window = None;

static void create_info_window(Client *c);
static void update_info_window(Client *c);
static void remove_info_window(void);
static void grab_keysym(Window w, unsigned int mask, KeySym keysym);

static void create_info_window(Client *c) {
	info_window = XCreateSimpleWindow(dpy, c->screen->root, -4, -4, 2, 2,
			0, c->screen->fg.pixel, c->screen->fg.pixel);
	XSetWindowAttributes attr;
	attr.save_under = True;
	XChangeWindowAttributes(dpy, info_window, CWSaveUnder, &attr);
	XMapRaised(dpy, info_window);
	update_info_window(c);
}

static void update_info_window(Client *c) {
	char *name;
	char buf[27];
	int namew, iwinx, iwiny, iwinw, iwinh;
	int width_inc = c->width_inc, height_inc = c->height_inc;

	if (!info_window)
		return;
	snprintf(buf, sizeof(buf), "%dx%d+%d+%d", (c->width-c->base_width)/width_inc,
		(c->height-c->base_height)/height_inc,
		client_to_Xcoord(c,x), client_to_Xcoord(c,y));
	iwinw = XTextWidth(font, buf, strlen(buf)) + 2;
	iwinh = font->max_bounds.ascent + font->max_bounds.descent;
	XFetchName(dpy, c->window, &name);
	if (name) {
		namew = XTextWidth(font, name, strlen(name));
		if (namew > iwinw)
			iwinw = namew + 2;
		iwinh = iwinh * 2;
	}
	iwinx = c->nx + c->border + c->width - iwinw;
	iwiny = c->ny - c->border;
	if (iwinx + iwinw > c->phy->width)
		iwinx = c->phy->width - iwinw;
	if (iwinx < 0)
		iwinx = 0;
	if (iwiny + iwinh > c->phy->height)
		iwiny = c->phy->height - iwinh;
	if (iwiny < 0)
		iwiny = 0;
	/* convert to X11 logical screen co-ordinates */
	iwinx += c->phy->xoff;
	iwiny += c->phy->yoff;
	XMoveResizeWindow(dpy, info_window, iwinx, iwiny, iwinw, iwinh);
	XClearWindow(dpy, info_window);
	if (name) {
		XDrawString(dpy, info_window, c->screen->invert_gc,
				1, iwinh / 2 - 1, name, strlen(name));
		XFree(name);
	}
	XDrawString(dpy, info_window, c->screen->invert_gc, 1, iwinh - 1,
			buf, strlen(buf));
}

static void remove_info_window(void) {
	if (info_window)
		XDestroyWindow(dpy, info_window);
	info_window = None;
}
#endif  /* INFOBANNER */

#if defined(MOUSE) || !defined(INFOBANNER)
static void draw_outline(Client *c) {
	int screen_x = client_to_Xcoord(c,x);
	int screen_y = client_to_Xcoord(c,y);
#ifndef INFOBANNER_MOVERESIZE
	char buf[27];
	int width_inc = c->width_inc, height_inc = c->height_inc;
#endif  /* ndef INFOBANNER_MOVERESIZE */

	XDrawRectangle(dpy, c->screen->root, c->screen->invert_gc,
		screen_x - c->border, screen_y - c->border,
		c->width + 2*c->border-1, c->height + 2*c->border-1);

#ifdef XINERAMA
	/* draw a cross-hair representing the client's centre of gravity */
	if (have_xinerama) {
		int cog_screen_x = screen_x + c->cog.x;
		int cog_screen_y = screen_y + c->cog.y;
		XDrawLine(dpy, c->screen->root, c->screen->invert_gc,
		          cog_screen_x - 4, cog_screen_y,
		          cog_screen_x + 5, cog_screen_y);
		XDrawLine(dpy, c->screen->root, c->screen->invert_gc,
		          cog_screen_x, cog_screen_y - 4,
		          cog_screen_x, cog_screen_y + 5);
	}
#endif
#ifndef INFOBANNER_MOVERESIZE
	snprintf(buf, sizeof(buf), "%dx%d+%d+%d", (c->width-c->base_width)/width_inc,
			(c->height-c->base_height)/height_inc, screen_x, screen_y);
	XDrawString(dpy, c->screen->root, c->screen->invert_gc,
		screen_x + c->width - XTextWidth(font, buf, strlen(buf)) - SPACE,
		screen_y + c->height - SPACE,
		buf, strlen(buf));
#endif  /* ndef INFOBANNER_MOVERESIZE */
}
#endif

#ifdef MOUSE
static void recalculate_sweep(Client *c, int x1, int y1, int x2, int y2) {
	if (c->oldw == 0) {
		c->width = abs(x1 - x2);
		c->width -= (c->width - c->base_width) % c->width_inc;
		if (c->min_width && c->width < c->min_width)
			c->width = c->min_width;
		if (c->max_width && c->width > c->max_width)
			c->width = c->max_width;
		c->nx = (x1 <= x2) ? x1 : x1 - c->width;
	}
	if (c->oldh == 0)  {
		c->height = abs(y1 - y2);
		c->height -= (c->height - c->base_height) % c->height_inc;
		if (c->min_height && c->height < c->min_height)
			c->height = c->min_height;
		if (c->max_height && c->height > c->max_height)
			c->height = c->max_height;
		c->ny = (y1 <= y2) ? y1 : y1 - c->height;
	}
}

void sweep(Client *c) {
	XEvent ev;
	int old_screen_origin_x = c->phy->xoff;
	int old_screen_origin_y = c->phy->yoff;
	int old_cx = c->nx;
	int old_cy = c->ny;

	if (!grab_pointer(c->screen->root, MouseMask, resize_curs)) return;

	client_raise(c);
#ifdef INFOBANNER_MOVERESIZE
	create_info_window(c);
#endif
	XGrabServer(dpy);
	draw_outline(c);

	setmouse(c->window, c->width, c->height);
	for (;;) {
		XMaskEvent(dpy, MouseMask, &ev);
		switch (ev.type) {
			case MotionNotify:
				if (ev.xmotion.root != c->screen->root)
					break;
				draw_outline(c); /* clear */
				XUngrabServer(dpy);
				/* normalize ev.xmotion to be in vdesk coordinates */
				ev.xmotion.x -= old_screen_origin_x;
				ev.xmotion.y -= old_screen_origin_y;
				recalculate_sweep(c, old_cx, old_cy, ev.xmotion.x, ev.xmotion.y);
				client_calc_cog(c);
				/* xxx: calc phy here */
#ifdef INFOBANNER_MOVERESIZE
				update_info_window(c);
#endif
				XSync(dpy, False);
				XGrabServer(dpy);
				draw_outline(c);
				break;
			case ButtonRelease:
				draw_outline(c); /* clear */
				XUngrabServer(dpy);
				client_calc_phy(c);
#ifdef INFOBANNER_MOVERESIZE
				remove_info_window();
#endif
				XUngrabPointer(dpy, CurrentTime);
				moveresize(c);
				return;
			default: break;
		}
	}
}
#endif

void show_info(Client *c, unsigned int keycode) {
	XEvent ev;
	XKeyboardState keyboard;

	if (XGrabKeyboard(dpy, c->screen->root, False, GrabModeAsync, GrabModeAsync, CurrentTime) != GrabSuccess)
		return;

	XGetKeyboardControl(dpy, &keyboard);
	XAutoRepeatOff(dpy);
#ifdef INFOBANNER
	create_info_window(c);
#else
	XGrabServer(dpy);
	draw_outline(c);
#endif
	do {
		XMaskEvent(dpy, KeyReleaseMask, &ev);
	} while (ev.xkey.keycode != keycode);
#ifdef INFOBANNER
	remove_info_window();
#else
	draw_outline(c);
	XUngrabServer(dpy);
#endif
	if (keyboard.global_auto_repeat == AutoRepeatModeOn)
		XAutoRepeatOn(dpy);
	XUngrabKeyboard(dpy, CurrentTime);
}

#ifdef MOUSE
static int absmin(int a, int b) {
	if (abs(a) < abs(b))
		return a;
	return b;
}

static void snap_client(Client *c) {
	(void)c;
	int dx, dy;
	Client *ci;
	/* client in screen co-ordinates */
	int c_screen_x = client_to_Xcoord(c,x);
	int c_screen_y = client_to_Xcoord(c,y);

	/* snap to other windows */
	dx = dy = opt_snap;
	for (struct dlist *iter = clients_mapping_order; iter; iter = iter->next) {
		ci = iter->data;
		int ci_screen_x = client_to_Xcoord(ci,x);
		int ci_screen_y = client_to_Xcoord(ci,y);

		if (ci == c) continue;
		if (ci->screen != ci->screen) continue;
#ifdef VWM
		if (!is_fixed(ci) && ci->vdesk != c->phy->vdesk) continue;
#endif
		if (ci->is_dock && !c->screen->docks_visible) continue;
		if (ci_screen_y - ci->border - c->border - c->height - c_screen_y <= opt_snap
		&& c_screen_y - c->border - ci->border - ci->height - ci_screen_y <= opt_snap) {
			dx = absmin(dx, ci_screen_x + ci->width - c_screen_x + c->border + ci->border);
			dx = absmin(dx, ci_screen_x + ci->width - c_screen_x - c->width);
			dx = absmin(dx, ci_screen_x - c_screen_x - c->width - c->border - ci->border);
			dx = absmin(dx, ci_screen_x - c_screen_x);
		}
		if (ci_screen_x - ci->border - c->border - c->width - c_screen_x <= opt_snap
		&& c_screen_x - c->border - ci->border - ci->width - ci_screen_x <= opt_snap) {
			dy = absmin(dy, ci_screen_y + ci->height - c_screen_y + c->border + ci->border);
			dy = absmin(dy, ci_screen_y + ci->height - c_screen_y - c->height);
			dy = absmin(dy, ci_screen_y - c_screen_y - c->height - c->border - ci->border);
			dy = absmin(dy, ci_screen_y - c_screen_y);
		}
	}
	if (abs(dx) < opt_snap)
		c->nx += dx;
	if (abs(dy) < opt_snap)
		c->ny += dy;

	/* snap to screen border */
	if (abs(c->nx - c->border) < opt_snap) c->nx = c->border;
	if (abs(c->ny - c->border) < opt_snap) c->ny = c->border;
	if (abs(c->nx + c->width + c->border - c->phy->width) < opt_snap)
		c->nx = c->phy->width - c->width - c->border;
	if (abs(c->ny + c->height + c->border - c->phy->height) < opt_snap)
		c->ny = c->phy->height - c->height - c->border;

	if (abs(c->nx) == c->border && c->width == c->phy->width)
		c->nx = 0;
	if (abs(c->ny) == c->border && c->height == c->phy->height)
		c->ny = 0;
}

void drag(Client *c) {
	XEvent ev;
	int x1, y1; /* pointer position at start of grab in screen co-ordinates */

	if (!grab_pointer(c->screen->root, MouseMask, move_curs)) return;
	client_raise(c);
	get_mouse_position(&x1, &y1, c->screen->root);
#ifdef INFOBANNER_MOVERESIZE
	create_info_window(c);
#endif
	if (no_solid_drag) {
		XGrabServer(dpy);
		draw_outline(c);
	}
	for (;;) {
		XMaskEvent(dpy, MouseMask, &ev);
		switch (ev.type) {
			case MotionNotify:
				if (ev.xmotion.root != c->screen->root)
					break;
				if (no_solid_drag) {
					draw_outline(c); /* clear */
					XUngrabServer(dpy);
				}
				c->nx += (ev.xmotion.x - x1);
				c->ny += (ev.xmotion.y - y1);
				x1 = ev.xmotion.x;
				y1 = ev.xmotion.y;
				client_calc_phy(c);
				if (opt_snap)
					snap_client(c);

#ifdef INFOBANNER_MOVERESIZE
				update_info_window(c);
#endif
				if (no_solid_drag) {
					XSync(dpy, False);
					XGrabServer(dpy);
					draw_outline(c);
				} else {
					XMoveWindow(dpy, c->parent,
						client_to_Xcoord(c,x) - c->border,
						client_to_Xcoord(c,y) - c->border);
					send_config(c);
				}
				break;
			case ButtonRelease:
				if (no_solid_drag) {
					draw_outline(c); /* clear */
					XUngrabServer(dpy);
				}
#ifdef INFOBANNER_MOVERESIZE
				remove_info_window();
#endif
				XUngrabPointer(dpy, CurrentTime);
				if (no_solid_drag) {
					moveresize(c);
				}
				return;
			default: break;
		}
	}
}
#endif /* def MOUSE */

void moveresize(Client *c) {
	client_raise(c);
	XMoveResizeWindow(dpy, c->parent,
			client_to_Xcoord(c,x) - c->border, client_to_Xcoord(c,y) - c->border,
			c->width, c->height);
	XMoveResizeWindow(dpy, c->window, 0, 0, c->width, c->height);
	send_config(c);
}

void maximise_client(Client *c, int action, int hv) {
	if (hv & MAXIMISE_HORZ) {
		if (c->oldw) {
			if (action == NET_WM_STATE_REMOVE
					|| action == NET_WM_STATE_TOGGLE) {
				c->nx = c->oldx;
				c->width = c->oldw;
				c->oldw = 0;
				XDeleteProperty(dpy, c->window, xa_evilwm_unmaximised_horz);
			}
		} else {
			if (action == NET_WM_STATE_ADD
					|| action == NET_WM_STATE_TOGGLE) {
				unsigned long props[2];
				c->oldx = c->nx;
				c->oldw = c->width;
				c->nx = 0;
				c->width = c->phy->width;
				props[0] = c->oldx;
				props[1] = c->oldw;
				XChangeProperty(dpy, c->window, xa_evilwm_unmaximised_horz,
						XA_CARDINAL, 32, PropModeReplace,
						(unsigned char *)&props, 2);
			}
		}
	}
	if (hv & MAXIMISE_VERT) {
		if (c->oldh) {
			if (action == NET_WM_STATE_REMOVE
					|| action == NET_WM_STATE_TOGGLE) {
				c->ny = c->oldy;
				c->height = c->oldh;
				c->oldh = 0;
				XDeleteProperty(dpy, c->window, xa_evilwm_unmaximised_vert);
			}
		} else {
			if (action == NET_WM_STATE_ADD
					|| action == NET_WM_STATE_TOGGLE) {
				unsigned long props[2];
				c->oldy = c->ny;
				c->oldh = c->height;
				c->ny = 0;
				c->height = c->phy->height;
				props[0] = c->oldy;
				props[1] = c->oldh;
				XChangeProperty(dpy, c->window, xa_evilwm_unmaximised_vert,
						XA_CARDINAL, 32, PropModeReplace,
						(unsigned char *)&props, 2);
			}
		}
	}
	/* xinerama: update the client's centre of gravity
	 *  NB, the client doesn't change physical screen */
	client_calc_cog(c);
	ewmh_set_net_wm_state(c);
	moveresize(c);
	discard_enter_events();
}

/**
 * tab_next:
 *  tab to next client, grab keyboard and repeat for each tab key event
 *
 * side-effects:
 *  - changes stacking order (by end of operation, selected client is raised)
 *  - updates the tab order to bring the chosen client to the front
 */
void tab_next(void) {
	/* first entry is currently selected client */
	struct dlist *tab_iter = clients_tab_order;
	if (!tab_iter) {
		/* no clients */
		return;
	}
	/* xxx: should probably tab to the first client if it doesn't
	 * have focus */

	ScreenInfo *s = find_current_screen();
	int grabok = XGrabKeyboard(dpy, s->root, False, GrabModeAsync, GrabModeAsync, CurrentTime);

	/* Retrieve&Build the current window stacking order.  This is used
	 * to restack the windows after each tabbing */
	Window dw, *wlist;
	unsigned nwins;
	XQueryTree(dpy, s->root, &dw, &dw, &wlist, &nwins);
	/* convert to required order for XRestackWindows */
	for (unsigned i = 0; i < nwins/2; i++) {
		Window tmp = wlist[nwins-i-1];
		wlist[nwins-i-1] = wlist[i];
		wlist[i] = tmp;
	}

	while (1) {
		tab_iter = policy_tab_iter(tab_iter, +1);
		if (!tab_iter)
			break;

		/* raise the window */
		Client *c = tab_iter->data;
		client_show(c);
		client_raise(c);
#ifdef WARP_POINTER
		setmouse(c->window, c->width + c->border - 1, c->height + c->border - 1);
#endif
		select_client(c);

		if (grabok != GrabSuccess)
			break;

		while (1) {
			XEvent ev;
			XMaskEvent(dpy, KeyPressMask|KeyReleaseMask, &ev);
			if (ev.type == KeyPress && XKeycodeToKeysym(dpy,ev.xkey.keycode,0) == KEY_NEXT)
				break;
			if (!(ev.type == KeyPress || XKeycodeToKeysym(dpy,ev.xkey.keycode,0) == KEY_NEXT))
				goto exit;
		}
		/* been asked to tab to next client, put the one we just
		 * tabbed to back from where it came */
		XRestackWindows(dpy, wlist, nwins);
	}

exit:
	if (grabok == GrabSuccess)
		XUngrabKeyboard(dpy, CurrentTime);
	XFree(wlist);
	discard_enter_events();
}

#ifdef VWM
/** switch_vdesk:
 *  Switch the virtual desktop on physical screen @p of logical screen @s
 *  to @v
 */
int switch_vdesk(ScreenInfo *s, PhysicalScreen *p, unsigned v) {
#ifdef DEBUG
	int hidden = 0, raised = 0;
#endif

	/* no-op if a physical screen is already displaying @v */
	for (unsigned i = 0; i < (unsigned) s->num_physical; i++) {
		if (v == s->physical[i].vdesk)
			return 0;
	}

	LOG_ENTER("switch_vdesk(screen=%d, from=%lu, to=%u)", s->screen, p->vdesk, v);
	if (current && !is_fixed(current)) {
		select_client(NULL);
	}
	for (struct dlist *iter = clients_mapping_order; iter; iter = iter->next) {
		Client *c = iter->data;
		if (c->screen != s)
			continue;
		if (c->vdesk == p->vdesk) {
			client_hide(c);
#ifdef DEBUG
			hidden++;
#endif
		} else if (c->vdesk == v) {
			/* NB, vdesk may not be on the same physical screen as previously,
			 * so move windows onto the physical screen */
			if (c->phy != p) {
				c->phy = p;
				moveresize(c);
			}
			if (!c->is_dock || s->docks_visible)
				client_show(c);
#ifdef DEBUG
			raised++;
#endif
		}
	}
	p->vdesk = v;
	ewmh_set_net_current_desktop(s);
	ewmh_set_evilwm_current_desktops(s);
	LOG_DEBUG("%d hidden, %d raised\n", hidden, raised);
	LOG_LEAVE();

	return 1;
}
#endif /* def VWM */

void set_docks_visible(ScreenInfo *s, int is_visible) {

	LOG_ENTER("set_docks_visible(screen=%d, is_visible=%d)", s->screen, is_visible);
	s->docks_visible = is_visible;
	for (struct dlist *iter = clients_mapping_order; iter; iter = iter->next) {
		Client *c = iter->data;
		if (c->screen != s)
			continue;
		if (c->is_dock) {
			if (is_visible) {
#ifdef VWM
				if (is_fixed(c) || (c->vdesk == c->phy->vdesk)) {
#endif
					client_show(c);
					client_raise(c);
#ifdef VWM
				}
#endif
			} else {
				client_hide(c);
			}
		}
	}
	LOG_LEAVE();
}

ScreenInfo *find_screen(Window root) {
	int i;
	for (i = 0; i < num_screens; i++) {
		if (screens[i].root == root)
			return &screens[i];
	}
	return NULL;
}

ScreenInfo *find_current_screen(void) {
	ScreenInfo *current_screen;
	find_current_screen_and_phy(&current_screen, NULL);
	return current_screen;
}

void find_current_screen_and_phy(ScreenInfo **current_screen, PhysicalScreen **current_phy) {
	Window cur_root, dw;
	int di;
	unsigned int dui;
	int x,y;

	/* XQueryPointer is useful for getting the current pointer root */
	XQueryPointer(dpy, screens[0].root, &cur_root, &dw, &x, &y, &di, &di, &dui);
	*current_screen = find_screen(cur_root);
	if (current_phy)
		*current_phy = find_physical_screen(*current_screen, x, y);
}

/** find_physical_screen:
 *   Given a logical screen, find which physical screen the point
 *   (@screen_x,@screen_y) resides.
 *
 *   If the point isn't on a physical screen, finds the closest screen
 *   centre.
 */
PhysicalScreen *find_physical_screen(ScreenInfo *screen, int screen_x, int screen_y) {
	PhysicalScreen *phy = NULL;

	/* Find if (screen_x,y) is on any physical screen */
	for (unsigned i = 0; i < (unsigned) screen->num_physical; i++) {
		phy = &screen->physical[i];
		if (screen_x >= phy->xoff && screen_x <= phy->xoff + phy->width
		&&  screen_y >= phy->yoff && screen_y <= phy->yoff + phy->height)
			return phy;
	}

	/* fall back to finding the closest screen minimum distance between the
	 * physical screen centre to (screen_x,y) */
	int val = INT_MAX;
	for (unsigned i = 0; i < (unsigned) screen->num_physical; i++) {
		PhysicalScreen *p = &screen->physical[i];

		int dx = screen_x - p->xoff - p->width/2;
		int dy = screen_y - p->yoff - p->height/2;

		if (dx * dx + dy * dy < val) {
			val = dx * dx + dy * dy;
			phy = p;
		}
	}
	return phy;
}

static void grab_keysym(Window w, unsigned int mask, KeySym keysym) {
	KeyCode keycode = XKeysymToKeycode(dpy, keysym);
	XGrabKey(dpy, keycode, mask, w, True,
			GrabModeAsync, GrabModeAsync);
	XGrabKey(dpy, keycode, mask|LockMask, w, True,
			GrabModeAsync, GrabModeAsync);
	if (numlockmask) {
		XGrabKey(dpy, keycode, mask|numlockmask, w, True,
				GrabModeAsync, GrabModeAsync);
		XGrabKey(dpy, keycode, mask|numlockmask|LockMask, w, True,
				GrabModeAsync, GrabModeAsync);
	}
}

static KeySym keys_to_grab[] = {
#ifdef VWM
	KEY_FIX, KEY_PREVDESK, KEY_NEXTDESK,
	XK_1, XK_2, XK_3, XK_4, XK_5, XK_6, XK_7, XK_8,
#endif
	KEY_NEW, KEY_KILL,
	KEY_TOPLEFT, KEY_TOPRIGHT, KEY_BOTTOMLEFT, KEY_BOTTOMRIGHT,
	KEY_LEFT, KEY_RIGHT, KEY_DOWN, KEY_UP,
	KEY_LOWER, KEY_ALTLOWER, KEY_INFO, KEY_MAXVERT, KEY_MAX,
	KEY_DOCK_TOGGLE
};
#define NUM_GRABS (int)(sizeof(keys_to_grab) / sizeof(KeySym))

static KeySym alt_keys_to_grab[] = {
	KEY_KILL, KEY_LEFT, KEY_RIGHT, KEY_DOWN, KEY_UP
};
#define NUM_ALT_GRABS (int)(sizeof(alt_keys_to_grab) / sizeof(KeySym))

void grab_keys_for_screen(ScreenInfo *s) {
	int i;
	/* Release any previous grabs */
	XUngrabKey(dpy, AnyKey, AnyModifier, s->root);
	/* Grab key combinations we're interested in */
	for (i = 0; i < NUM_GRABS; i++) {
		grab_keysym(s->root, grabmask1, keys_to_grab[i]);
	}
	for (i = 0; i < NUM_ALT_GRABS; i++) {
		grab_keysym(s->root, grabmask1 | altmask, alt_keys_to_grab[i]);
	}
	grab_keysym(s->root, grabmask2, KEY_NEXT);
}



/**
 **
 ** Policy section, to move elsewhere.
 **
 */

/** tab_iter:
 *   choose the next client to tab to
 *  @direction<0 -> backwards
 *  @direction>0 -> forwards
 */
/* todo, convert to clist */
struct dlist *policy_tab_iter(struct dlist *iter, int direction) {
	if(!iter) return NULL;

	/* record the starting point, so we know to give up if
	 * unable to choose anything else before getting back */
	/* xxx: heffalumps and woozles */
	struct dlist *start = iter;
	struct dlist *chosen = NULL;
	do {
		iter = (direction > 0) ? iter->next : iter->prev;
		if (!iter) {
			if (direction > 0) {
				/* cycle round to the start of the list */
				iter = clients_tab_order;
			} else {
				/* cycle round to the end of the list */
				iter = dlist_tail(clients_tab_order);
			}
		}
		Client *c = iter->data;
		/* filter out the unwanted clients */
		/* currently: allscreens(unmapped(hidden, wrongvdesk), mapped(all visible vdesks)) */
		if (!should_be_mapped(c)) {
			continue;
		}
		/* currently: allscreens(mapped(all visible vdesks)) */
		chosen = iter;
		break;
	} while (iter != start);

	return chosen;
}
