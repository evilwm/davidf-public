/* evilwm - Minimalist Window Manager for X
 * Copyright (C) 1999-2009 Ciaran Anscomb <evilwm@6809.org.uk>
 * see README for license and other details. */

#ifndef __LIST_H__
#define __LIST_H__

struct dlist {
	struct dlist *next;
	struct dlist *prev;
	void *data;
};

/* Each of these return the new pointer to the head of the list: */
struct dlist *dlist_prepend(struct dlist *list, void *data);
struct dlist *dlist_append(struct dlist *list, void *data);
struct dlist *dlist_delete(struct dlist *list, void *data);
struct dlist *dlist_to_head(struct dlist *list, void *data);
struct dlist *dlist_to_tail(struct dlist *list, void *data);

/* Returns element in list: */
struct dlist *dlist_find(struct dlist *list, void *data);
struct dlist *dlist_tail(struct dlist *list);

/* misc */
unsigned dlist_length(struct dlist *list);

#endif  /* def __LIST_H__ */
