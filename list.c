/* evilwm - Minimalist Window Manager for X
 * Copyright (C) 1999-2009 Ciaran Anscomb <evilwm@6809.org.uk>
 * see README for license and other details. */

/* Basic linked list handling code.  Operations that modify the list
 * return the new list head.  */

#include <stdlib.h>
#include "list.h"

static struct dlist *dlist_delete1(struct dlist *list, struct dlist *elem);

/* Wrap data in a new list container */
static struct dlist *dlist_new(void *data) {
	struct dlist *new;
	new = malloc(sizeof(struct dlist));
	if (!new) return NULL;
	new->next = NULL;
	new->prev = NULL;
	new->data = data;
	return new;
}

/* return the length of a dlist */
unsigned dlist_length(struct dlist *list) {
	unsigned len = 0;
	for (struct dlist *iter = list; iter; iter = iter->next)
		len++;
	return len;
}

/* Add new data to head of dlist */
struct dlist *dlist_prepend(struct dlist *list, void *data) {
	struct dlist *elem = dlist_new(data);
	if (!elem) return list;
	elem->next = list;
	if (list)
		list->prev = elem;
	return elem;
}

/* Add new data to tail of dlist */
struct dlist *dlist_append(struct dlist *list, void *data) {
	struct dlist *elem = dlist_new(data);
	if (!elem) return list;
	if (!list) return elem;
	struct dlist *tail = dlist_tail(list);
	tail->next = elem;
	elem->prev = tail;
	return list;
}

/* Delete given list element (returns list head) */
static struct dlist *dlist_delete1(struct dlist *list, struct dlist *elem) {
	if (!list) return NULL;
	if (!elem) return list;

	if (elem->prev) {
		elem->prev->next = elem->next;
	} else {
		/* no previous implies this was head */
		list = elem->next;
	}
	if (elem->next) {
		elem->next->prev = elem->prev;
	}
	free(elem);
	return list;
}

/* Delete list element containing data */
struct dlist *dlist_delete(struct dlist *list, void *data) {
	return dlist_delete1(list, dlist_find(list, data));
}

/* Move existing list element containing data to head of list */
struct dlist *dlist_to_head(struct dlist *list, void *data) {
	if (!data) return list;
	list = dlist_delete(list, data);
	return dlist_prepend(list, data);
}

/* Move existing list element containing data to tail of list */
struct dlist *dlist_to_tail(struct dlist *list, void *data) {
	if (!data) return list;
	list = dlist_delete(list, data);
	return dlist_append(list, data);
}

/* Find list element containing data */
struct dlist *dlist_find(struct dlist *list, void *data) {
	struct dlist *elem;
	for (elem = list; elem; elem = elem->next) {
		if (elem->data == data)
			return elem;
	}
	return NULL;
}

/* Find the last element in a list */
struct dlist *dlist_tail(struct dlist *list) {
	struct dlist *elem;
	if (!list) return NULL;
	for (elem = list; elem->next; elem = elem->next);
	return elem;
}
